package com.gdaditya.portfolioservice.util;

import com.gdaditya.portfolioservice.model.DatabaseSequence;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Component
@RequiredArgsConstructor
public class MongoUtil {

    private final MongoOperations mongoOperations;

    public long generateSequence(String seqName) {
        DatabaseSequence counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
                new Update().inc("seq",1), options().returnNew(true).upsert(true),
                DatabaseSequence.class);
        return counter == null ? 1 : counter.getSeq();
    }

    public void resetSequence(String seqName) {
        mongoOperations.findAndModify(
                query(where("_id").is(seqName)),
                new Update().set("seq", 0),
                DatabaseSequence.class
        );
    }
}
