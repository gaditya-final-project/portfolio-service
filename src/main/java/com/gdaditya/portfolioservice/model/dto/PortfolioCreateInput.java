package com.gdaditya.portfolioservice.model.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PortfolioCreateInput {
    @NotEmpty(message = "Title cannot be empty!")
    private String title;
    @NotNull(message = "Portfolio type ID cannot be empty!")
    private Long portfolioTypeId;
    @NotEmpty(message = "Description cannot be empty!")
    private String description;
}
