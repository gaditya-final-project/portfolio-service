package com.gdaditya.portfolioservice.model.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PortfolioUpdateInput {
    @NotNull(message = "ID cannot be empty!")
    private Long id;
    private String title;
    private Long portfolioTypeId;
    private String description;
}
