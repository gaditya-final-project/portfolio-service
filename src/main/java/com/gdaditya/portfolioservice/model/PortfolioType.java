package com.gdaditya.portfolioservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document("portofolio_type")
public class PortfolioType {

    @Transient
    public static final String SEQUENCE_NAME = "portfolio_type_sequence";

    @Id
    private Long id;
    private String name;
    @CreatedDate
    @JsonIgnore
    private Date createdAt;
    @LastModifiedDate
    @JsonIgnore
    private Date updatedAt;

    public PortfolioType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

}
