package com.gdaditya.portfolioservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@Document("portfolio")
public class Portfolio {

    @Transient
    public static final String SEQUENCE_NAME = "portfolio_sequence";

    @Id
    private Long id;
    @DBRef
    private User user;
    @DBRef
    private PortfolioType portfolioType;
    private String title;
    private String description;
    @CreatedDate
    private Date createdAt;
    @LastModifiedDate
    private Date updatedAt;
}
