package com.gdaditya.portfolioservice.config;

import com.gdaditya.portfolioservice.model.DatabaseSequence;
import com.gdaditya.portfolioservice.model.PortfolioType;
import com.gdaditya.portfolioservice.util.MongoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@RequiredArgsConstructor
public class PortfolioConfig {

    private final MongoTemplate mongoTemplate;
    private final MongoUtil mongoUtil;

    @Bean
    public ApplicationRunner createPortfolioTypes() {
        return args -> {
            mongoTemplate.dropCollection(DatabaseSequence.class);
            mongoUtil.resetSequence(PortfolioType.SEQUENCE_NAME);
            mongoTemplate.save(new PortfolioType(mongoUtil.generateSequence(PortfolioType.SEQUENCE_NAME), "Learning"));
            mongoTemplate.save(new PortfolioType(mongoUtil.generateSequence(PortfolioType.SEQUENCE_NAME), "Projects"));
            mongoTemplate.save(new PortfolioType(mongoUtil.generateSequence(PortfolioType.SEQUENCE_NAME), "Knowledge"));
            mongoTemplate.save(new PortfolioType(mongoUtil.generateSequence(PortfolioType.SEQUENCE_NAME), "Experience"));
        };
    }

}
