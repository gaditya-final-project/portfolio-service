package com.gdaditya.portfolioservice.service;

import com.gdaditya.portfolioservice.exception.BaseException;
import com.gdaditya.portfolioservice.exception.JwtTokenMalformedException;
import com.gdaditya.portfolioservice.exception.JwtTokenMissingException;
import com.gdaditya.portfolioservice.model.Portfolio;
import com.gdaditya.portfolioservice.model.dto.PortfolioCreateInput;
import com.gdaditya.portfolioservice.model.dto.PortfolioUpdateInput;

import java.util.List;

public interface PortfolioService {
    Portfolio createPortfolio(PortfolioCreateInput input, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    Portfolio getFromId(Long id, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    List<Portfolio> getAllFromUserId(Long userId, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    List<Portfolio> getAllFromPortfolioTypeId(Long portfolioTypeId) throws BaseException;
    List<Portfolio> getAll();
    Portfolio updatePortfolio(PortfolioUpdateInput input, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
    void deletePortfolioFromId(Long id, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;

}
