package com.gdaditya.portfolioservice.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gdaditya.portfolioservice.exception.BaseException;
import com.gdaditya.portfolioservice.exception.JwtTokenMalformedException;
import com.gdaditya.portfolioservice.exception.JwtTokenMissingException;
import com.gdaditya.portfolioservice.model.User;
import com.gdaditya.portfolioservice.model.dto.BaseResponse;
import com.gdaditya.portfolioservice.service.UserService;
import com.gdaditya.portfolioservice.util.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final MongoTemplate mongoTemplate;
    private final RestTemplate restTemplate;
    private final JwtUtil jwtUtil;
    private final ObjectMapper mapper;
    private static final String USER_ADDR = "http://auth-service/api/user/";

    public User getUserFromId(Long id) throws BaseException {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(jwtUtil.generateApiToken());
        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        ResponseEntity<BaseResponse> resp = null;
        try {
            resp = restTemplate.exchange(USER_ADDR+id, HttpMethod.GET, entity, BaseResponse.class);
        } catch (Exception e) {
            if (e instanceof HttpClientErrorException.NotFound)
                throw new BaseException(HttpStatus.BAD_REQUEST, "User not found!");
            log.warn(e);
        }
        if (resp != null && resp.getStatusCode() == HttpStatus.OK && resp.getBody() != null) {
            User user = mapper.convertValue(resp.getBody().getData(), User.class);
            return mongoTemplate.save(user);
        } else
            throw new BaseException(HttpStatus.INTERNAL_SERVER_ERROR, "Error communicating to user service.");
    }

    @Override
    public User getUserFromToken(String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        Claims claims = jwtUtil.getClaims(token);
        User user =  User.builder()
                .id(Long.parseLong(String.valueOf(claims.get("id"))))
                .email(String.valueOf(claims.get("email")))
                .username(String.valueOf(claims.get("username")))
                .build();
        mongoTemplate.save(user);
        return user;
    }
}
