package com.gdaditya.portfolioservice.service.impl;

import com.gdaditya.portfolioservice.exception.BaseException;
import com.gdaditya.portfolioservice.exception.JwtTokenMalformedException;
import com.gdaditya.portfolioservice.exception.JwtTokenMissingException;
import com.gdaditya.portfolioservice.exception.UnauthorizedException;
import com.gdaditya.portfolioservice.model.Portfolio;
import com.gdaditya.portfolioservice.model.PortfolioType;
import com.gdaditya.portfolioservice.model.User;
import com.gdaditya.portfolioservice.model.dto.PortfolioCreateInput;
import com.gdaditya.portfolioservice.model.dto.PortfolioUpdateInput;
import com.gdaditya.portfolioservice.service.PortfolioService;
import com.gdaditya.portfolioservice.service.UserService;
import com.gdaditya.portfolioservice.util.JwtUtil;
import com.gdaditya.portfolioservice.util.MongoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
@RequiredArgsConstructor
public class PortfolioServiceImpl implements PortfolioService {

    private final UserService userService;
    private final MongoTemplate mongoTemplate;
    private final MongoUtil mongoUtil;
    private final JwtUtil jwtUtil;


    @Override
    public Portfolio createPortfolio(PortfolioCreateInput input, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        User user = userService.getUserFromToken(token);
        PortfolioType portfolioType = getPortfolioTypeFromId(input.getPortfolioTypeId());
        Portfolio portfolio = Portfolio.builder()
                .id(mongoUtil.generateSequence(Portfolio.SEQUENCE_NAME))
                .user(user)
                .portfolioType(portfolioType)
                .title(input.getTitle())
                .description(input.getDescription())
                .build();
        return mongoTemplate.save(portfolio, "portfolio");
    }

    @Override
    public Portfolio getFromId(Long id, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Portfolio portfolio = mongoTemplate.findOne(
                Query.query(Criteria.where("id").is(id)),
                Portfolio.class
        );
        String role = jwtUtil.getUserRole(token);
        Long userId = jwtUtil.getUserId(token);
        if (portfolio == null)
            throw new BaseException(HttpStatus.NOT_FOUND, "Portfolio with id " + id + " not found!");
        if (!Objects.equals(role, "ADMIN") && !portfolio.getUser().getId().equals(userId))
            throw new UnauthorizedException();
        return portfolio;
    }

    @Override
    public List<Portfolio> getAllFromUserId(Long userId, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        String role = jwtUtil.getUserRole(token);
        Long userId1 = jwtUtil.getUserId(token);
        if (!Objects.equals(role, "ADMIN") && !userId.equals(userId1))
            throw new UnauthorizedException();
        User user = userService.getUserFromId(userId);
        return mongoTemplate.find(
                Query.query(Criteria.where("user").is(user)),
                Portfolio.class
        );
    }

    @Override
    public List<Portfolio> getAllFromPortfolioTypeId(Long portfolioTypeId) throws BaseException {
        PortfolioType portfolioType = getPortfolioTypeFromId(portfolioTypeId);
        return mongoTemplate.find(
                Query.query(Criteria.where("portfolioType").is(portfolioType)),
                Portfolio.class
        );
    }

    @Override
    public List<Portfolio> getAll() {
        return mongoTemplate.findAll(Portfolio.class);
    }

    @Override
    public Portfolio updatePortfolio(PortfolioUpdateInput input, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Portfolio portfolio = getFromId(input.getId(), token);
        if (input.getTitle() != null)
            portfolio.setTitle(input.getTitle());
        if (input.getPortfolioTypeId() != null) {
            PortfolioType portfolioType = getPortfolioTypeFromId(input.getPortfolioTypeId());
            portfolio.setPortfolioType(portfolioType);
        } if (input.getDescription() != null)
            portfolio.setDescription(input.getDescription());
        return mongoTemplate.save(portfolio);
    }

    @Override
    public void deletePortfolioFromId(Long id, String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Portfolio portfolio = getFromId(id, token);
        mongoTemplate.remove(portfolio);
    }

    private PortfolioType getPortfolioTypeFromId(Long id) throws BaseException {
        PortfolioType portfolioType = mongoTemplate.findOne(
                Query.query(Criteria.where("id").is(id)), PortfolioType.class);
        if (portfolioType == null)
            throw new BaseException(HttpStatus.BAD_REQUEST, "Portfolio type with ID " + id + " does not exist!");
        return portfolioType;
    }
}
