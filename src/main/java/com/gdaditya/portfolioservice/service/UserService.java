package com.gdaditya.portfolioservice.service;

import com.gdaditya.portfolioservice.exception.BaseException;
import com.gdaditya.portfolioservice.exception.JwtTokenMalformedException;
import com.gdaditya.portfolioservice.exception.JwtTokenMissingException;
import com.gdaditya.portfolioservice.model.User;

public interface UserService {
    User getUserFromId(Long id) throws BaseException;
    User getUserFromToken(String token) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException;
}
