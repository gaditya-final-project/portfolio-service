package com.gdaditya.portfolioservice.controller;

import com.gdaditya.portfolioservice.exception.BaseException;
import com.gdaditya.portfolioservice.exception.JwtTokenMalformedException;
import com.gdaditya.portfolioservice.exception.JwtTokenMissingException;
import com.gdaditya.portfolioservice.exception.UnauthorizedException;
import com.gdaditya.portfolioservice.model.dto.BaseResponse;
import com.gdaditya.portfolioservice.model.dto.PortfolioCreateInput;
import com.gdaditya.portfolioservice.model.dto.PortfolioUpdateInput;
import com.gdaditya.portfolioservice.service.PortfolioService;
import com.gdaditya.portfolioservice.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/portfolio")
@RequiredArgsConstructor
public class PortfolioController {

    private final PortfolioService portfolioService;
    private final JwtUtil jwtUtil;

    @PostMapping("/")
    public ResponseEntity<?> createPortfolio(
            @RequestBody @Valid PortfolioCreateInput portfolioCreateInput,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.createPortfolio(portfolioCreateInput, token)));
    }

    @GetMapping("/")
    public ResponseEntity<?> getCreatedPortfolio(
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        Long id = jwtUtil.getUserId(token);
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.getAllFromUserId(id, token)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPortfolioFromId(
            @PathVariable("id") Long id,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.getFromId(id, token)));
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getPortfolioFromUserId(
            @PathVariable("id") Long id,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.getAllFromUserId(id, token)));
    }

    @GetMapping("/type/{id}")
    public ResponseEntity<?> getPortfolioFromTypeId(
            @PathVariable("id") Long id,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.getAllFromPortfolioTypeId(id)));
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllPortfolio(
            @RequestHeader("Authorization") String token
    ) throws JwtTokenMalformedException, JwtTokenMissingException, UnauthorizedException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.getAll()));
    }

    @PatchMapping("/")
    public ResponseEntity<?> updatePortfolio(
            @RequestBody @Valid PortfolioUpdateInput portfolioUpdateInput,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(portfolioService.updatePortfolio(portfolioUpdateInput, token)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePortfolio(
            @PathVariable("id") Long id,
            @RequestHeader("Authorization") String token
    ) throws BaseException, JwtTokenMalformedException, JwtTokenMissingException {
        portfolioService.deletePortfolioFromId(id, token);
        return ResponseEntity.ok(new BaseResponse<>());
    }

    private boolean isTokenNotRole(String token, String role) throws JwtTokenMalformedException, JwtTokenMissingException {
        return !jwtUtil.getUserRole(token).equals(role);
    }
}
